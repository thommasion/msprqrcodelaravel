<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ReductionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reductions = [
            '1' => [
                'reducHash' => Str::random(20),
                'pourcentReduc' => 20,
                'dateValidation' => '2021-04-20',
                'archive' => 0
            ],
            '2' => [
                'reducHash' => Str::random(20),
                'pourcentReduc' => 5,
                'dateValidation' => '2021-04-06',
                'archive' => 0
            ],
            '3' => [
                'reducHash' => Str::random(20),
                'pourcentReduc' => 40,
                'dateValidation' => '2021-04-30',
                'archive' => 0
            ],
            '4' => [
                'reducHash' => Str::random(20),
                'pourcentReduc' => 15,
                'dateValidation' => '2021-05-01',
                'archive' => 0
            ]
        ];
        foreach ($reductions as $reduction) {
            DB::table('reductions')->insert($reduction);
        }
    }
}

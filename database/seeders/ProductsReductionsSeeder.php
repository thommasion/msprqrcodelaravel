<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsReductionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productsReductions = [
            '1' => [
                'productId' => 1,
                'reductionId' => 4
            ],
            '2' => [
                'productId' => 2,
                'reductionId' => 1
            ],
            '3' => [
                'productId' => 2,
                'reductionId' => 3
            ],
            '4' => [
                'productId' => 3,
                'reductionId' => 3
            ],
            '5' => [
                'productId' => 4,
                'reductionId' => 4
            ],
            '6' => [
                'productId' => 4,
                'reductionId' => 2
            ]
        ];
        foreach ($productsReductions as $productReduction) {
            DB::table('productsreductions')->insert($productReduction);
        }
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            '1' => [
                'nom' => 'PC Portable Acer',
                'prix' => '4000'
            ],
            '2' => [
                'nom' => 'Etui de lunettes',
                'prix' => '30'
            ],
            '3' => [
                'nom' => 'Passoire',
                'prix' => '10'
            ],
            '4' => [
                'nom' => 'Baton',
                'prix' => '50'
            ]
        ];
        foreach ($products as $product) {
            DB::table('products')->insert($product);
        }
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductsReductions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ProductsReductions', function (Blueprint $table) {
            $table->foreignId('productId');
            $table->foreignId('reductionId');
            $table->foreign('productId')->references('productId')->on('products');
            $table->foreign('reductionId')->references('reductionId')->on('reductions');
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ProductsReductions');
    }
}
